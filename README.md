### Deprecated, please go to https://git.wur.nl/rdm-infrastructure/irods-training

Extracts from and updates to the existing EUDAT training:
https://github.com/EUDAT-Training/B2SAFE-B2STAGE-Training

File | Target audience | Status
------|--------------|-----
<span class="css-truncate css-truncate-target"><a href="/01-install-iRODS4.md" class="js-navigation-open" title="01-install-iRODS4.md">01-install-iRODS4.md</a></span> | sysadmins
<span class="css-truncate css-truncate-target"><a href="/02-iRODS-federation.md" class="js-navigation-open" title="02-iRODS-federation.md">02-iRODS-federation.md</a></span> | sysadmins, technical data stewards
<span class="css-truncate css-truncate-target"><a href="/03-Install-iRODS-resource-server.md" class="js-navigation-open" title="03-Install-iRODS-resource-server.md">03-Install-iRODS-resource-server.md</a></span>	| sysadmins
<span class="css-truncate css-truncate-target"><a href="/04-Training-Setup.md" class="js-navigation-open" title="04-Training-Setup.md">04-Training-Setup.md</a></span>	| sysadmins, trainers from  our team
<span class="css-truncate css-truncate-target"><a href="/05-icommands-for-users.md" class="js-navigation-open"  title="05-icommands-for-users.md">05-icommands-for-users.md</a></span>	| sysadmins, scientific programmers, researchers
<span class="css-truncate css-truncate-target"><a href="/06-icommands-for-admins.md" class="js-navigation-open" title="06-icommands-for-admins.md">05-iRODS-advanced-users.md</a></span>| technical data stewards, researchers
<span class="css-truncate css-truncate-target"><a href="/07-python-API-for-users.md" class="js-navigation-open" title="07-python-API-for-users.md">07-python-API-for-users.md</a><br><a href="/07-python-API-for-users.ipynb" class="js-navigation-open" title="07-python-API-for-users.ipynb">07-python-API-for-users.ipynb</a></span>| scientific programmers, researchers
<span class="css-truncate css-truncate-target"><a href="/08-Davrods-install.md " class="js-navigation-open" title="08-Davrods-install.md ">08-Davrods-install.md </a></span> | sysadmins
<span class="css-truncate css-truncate-target"><a href="/09-advanced-users.md " class="js-navigation-open" title="09-advanced-users.md ">09-advanced-users.md </a></span> | technical data stewards
<span class="css-truncate css-truncate-target"><a href="/10-SSL-encryption-and-PAM.md " class="js-navigation-open" title="10-SSL-encryption-and-PAM.md ">10-SSL-encryption-and-PAM.md </a></span> | sysadmins
<span class="css-truncate css-truncate-target"><a href="/11-Advanced-rules.md " class="js-navigation-open" title="11-Advanced-rules.md ">11-Advanced-rules.md </a></span> | sysadmins, technical data stewards | in development
<span class="css-truncate css-truncate-target"><a href="/12-S3-resources.md " class="js-navigation-open" title="12-S3-resources.md ">12-S3-resources.md </a></span> | sysadmins
<span class="css-truncate css-truncate-target"><a href="/13-HPC-RDM-Training " class="js-navigation-open" title=" 13-HPC-RDM-Training "> 13-HPC-RDM-Training </a></span> | scientific programmers, data stewards 


Enjoy the training.
